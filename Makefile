
CC=gcc
CFLAGS=-W -Wall -O2
LIBS=-lmhash

cd5: cd5.c
	$(CC) $(CFLAGS) cd5.c -o cd5 $(LIBS)

clean:
	$(RM) cd5 cd5.o

